"""
Your auntie gives you a puzzle for your birthday. It's a box with one button and a screen. 
Intrigued by it, you press the button and see an "X" appear on the screen.
You press the button, again, and the "X" is replaced by "O". You press it, again, and the "O" is replaced by "OX".
You keep pressing it and get "OXO", "OXOOX".

After playing around with it for a while, you realize that every time you press the
button, it changes all of the "X" letters to "O" letters, and changes all of the "O" letters
to the letters "OX".

Here's that sequence again:
Start:
Press: X
Press: O      (The X becomes an O)
Press: OX     (The O becomes OX)
Press: OXO    (Of OX, the first O becomes OX and the X
               becomes an O)
Press: OXOOX  (Of OXO,
                the first O becomes OX,
                the X becomes O, and
                the second O becomes OX)

The sequence has you intrigued. You decide to write a program to figure out how
many "X" and "O" letters there are on the screen after you push it a certain number of times.

---------------------------------------------------PROMPT------------------------------------------------------------------
Given the number of times to push the button num_pushes, complete the
function calculate_num_letters so that it returns both the number of Xs and
Os. The placeholder function shows you how that works.

Constraints: 1 <= num_pushes <= 20
"""

def calculate_num_letters(num_pushes):
    num_x = 1
    num_o = 0

    for i in range(1, num_pushes):
        num_x, num_o = num_o, num_x + num_o
    return num_x, num_o