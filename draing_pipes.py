"""
One day, Rube Goldberg  was asked to design the drain pipes for an apartment
building. He did so with his usual creativity. Then, rather than turning in a diagram to
his benefactors, he gave them some lists of numbers.

"The first number", he said, "is the number of apartments in the building. Each can
handle eight gallons of water per minute. The rest of the numbers are when pipes
split or combine."

"If you see one number," he continued, "then that pipe joins the pipe to its right.

"If you see two numbers, then the first number is the number of the pipe splits into
two pipes. The second number is how much of the flow goes into the left pipe. The
remainder will go into the second pipe.


"""


#-------------------------------SOLUTION----------------------------------------
def pipe_outputs(num_pipes, steps):
    #create list of pipes
    pipes = [8 for x in range(num_pipes)]
    for step in steps:
        if len(step) == 1:
            #add the value of the left pipe to the right
            pipes[step[0]] += pipes[step[0]-1]
            #delete the left pipe
            pipes.pop(step[0]-1)
        elif len(step) == 2:
            #subtract the flow amount from the pipe at the specified index
            pipes[step[0]-1] -= step[1]
            #insert a new pipe with the flow amount specified
            pipes.insert(step[0]-1, step[1])
    return pipes

#-------------------------------OTHER SOLUTION----------------------------------------
def pipe_outputs(num_pipes, steps):
    current_pipes = []
    for i in range(num_pipes):
        current_pipes.append(8)

    # loop over steps
    for step in steps:
        # check if we're splitting or comboing
        if len(step) == 1:
            # get left
            # get right
            # add
            # remove right
            # overwrite left
            left = current_pipes[step[0] - 1]
            current_pipes[step[0]] += left
            current_pipes.pop(step[0] - 1)
        elif len(step) == 2:
            # use index of step, this is what we want to update
            # pipe_total = above val
            # update at index with step[0]
            # insert at index with step[0], val = pipe_total - step[1]
            update_i = step[0] - 1
            pipe_total = current_pipes[update_i]
            current_pipes[update_i] = step[1]
            current_pipes.insert(step[0], pipe_total - step[1])

    return current_pipes