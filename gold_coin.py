"""
There's an old game played by a group of kids. One gets a chocolate gold coin, but keeps it secret.
Then, all the kids in the group play rock-paper-scissors.
If you're holding the gold coin and your opponent wins rock-paper-scissors, then you surreptitiously give the gold coin to them.
Otherwise, you hold on to the gold coin.

As many things do, this game has taken the adult world by storm.
You're going to be on the leading edge of the fad with the first tournament Web site set up to track the gold coins tournaments.

---------------------------------------------------PROMPT------------------------------------------------------------------
Each round of the game has 26 players, labeled "A" through "Z".
You're given the starting_player who has the gold coin, and a list of matches where each
entry is a string of two letters denoting the two players in the match, the first the
winner and the second the lower. 
Complete the function outcome to return the label of the player that has the gold coin at the end of the tournament.

Constraints: 1 <= len(matches) <= 100

Example:
Your function gets the value "Z" for starting_player, which means "Z" has the gold coin. For matches, your input list looks like this:

[ "ZA", "CD", "BZ", "SE", "FA", "LB"]

First match: ZA - Z wins and keeps the coin
Second match: CD - neither has the coin
Third match: BZ - B wins the coin from Z
Fourth match: SE - neither has the coin
Fifth match: FA - neither has the coin
Sixth match: LB - L wins the coin from B

Your function should return "L" for that input.
"""
def outcome(starting_player, matches):
    winning_bruh = starting_player

    for bruh in matches:
        if bruh[1] == winning_bruh:
            winning_bruh = bruh[0]
            
    return winning_bruh