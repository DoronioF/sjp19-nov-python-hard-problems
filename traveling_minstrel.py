"""
Every evening villagers in a small village gather around a big fire and 
sing songs.

A visitor to the community is the traveling minstrel. 
Every evening, if the minstrel is present, he sings a brand new song that no villager has heard before, and no other song is sung that night. 
In the event that the minstrel is not present, other villagers sing without him and exchange all songs that they know.

Given the list of villagers present for some number of consecutive evenings, you should calculate the total number of villagers that know all of the songs.

For example, let's say there are 5 villagers in the village, numbered 1, 2, 3, 4, and 5. Let's say the minstrel is represented by the number 0. Here's a record of each evening:

[Evening | Attendees | Who knows all the songs |            Reason                 ]
[   1    |    2, 4   |          2, 4           |     2 and 4 know all the songs    ]
[   2    |  0, 1, 2  |          2              |   Only 2 knows all the songs sung ]
[   3    |  1, 3, 4  |        1, 2, 3, 4       | 1 knows the song from evening 2, and four knows the songs from evening 1, sharing them they now know all the songs]
[   4    |    1, 2   |        1, 2, 3, 4       |       	Nothing changes            ]
[   5    |    0, 5   |                         |   5 now knows a new song that no one else knows and doesn't know any other songs ]
[   6    |    3, 5   |          3, 5           |   3 and 5 share their songs and now know all of them   ]

The result is 2, just the villabers 3 and 5 know all of the songs.

Given the number of villagers num_villagers and a list of lists attendees_lists
that record the attendees each evening, where 0 is the minstrel 
"""
#----------------------Notes--------------------------------------------------------------
"""
When the minstrel is present a new song is introduced and no one passes on the previous songs
When the minstrel is not present the current attendees pass on the songs they know
Assume that on evening 1 a minstrel is present

Create a dictionary to keep track of how many songs a vilager knows
Track when a minstrel introduces a new song
Check for when there is no minstrel in the attendees list
Return which villagers know all the songs at the end
"""
def total_song_knowers(num_villagers, attendees_lists):
    vil_count = {i: 0 for i in range(num_villagers + 1)}
    result = 0
    
    for index, evening in enumerate(attendees_lists):
        if 0 in evening:
            for bruh in evening:
                vil_count[bruh] += 1
        for attendee in evening:
            if attendee in evening and 0 not in evening:
                vil_count[attendee] += 1
                if attendee in attendees_lists[index - 1]:
                    vil_count[attendee] -= 1
    for k, v in vil_count.items():
        if v >= vil_count[0] and k != 0:
            result += 1
                
    return result

test1_vil = 3
test1_lists = [[0, 1], [1, 2, 3], [3, 1, 0]]
test2_vil = 4
test2_lists = [[0, 2], [1, 0], [1, 0, 3, 4]]
test3_vil = 7
test3_lists = [[0, 2, 4, 3], [4, 5], [5, 6, 7], [5, 1], [1, 5, 7, 0]]

print(total_song_knowers(test1_vil, test1_lists))

#------------------------------Proper Solution-------------------------------------------------

def total_song_knowers(num_villagers, attendees_lists):
    songs_known_by_villager = [None]
    num_minstrel_songs = 0
    for _ in range(num_villagers):
        songs_known_by_villager.append([])
    for evening_num in range(len(attendees_lists)):
        attendees_list = attendees_lists[evening_num]
        if 0 in attendees_list:
            num_minstrel_songs += 1
            for attendee in attendees_list:
                if attendee == 0:
                    continue
                songs_known_by_villager[attendee].append(evening_num)
        else:
            shared_songs = set()
            for attendee in attendees_list:
                shared_songs = shared_songs.union(songs_known_by_villager[attendee])
            for attendee in attendees_list:
                songs_known_by_villager[attendee] = list(shared_songs)
    count = 0
    for song_list in songs_known_by_villager[1:]:
        if len(song_list) == num_minstrel_songs:
            count += 1
    return count

#-------------------------------Trash Solution-------------------------------------------------

def total_song_knowers(num_villagers, attendees_lists):
    
    vill_dict = {}
    song_list = []
    total_songs = 0
    best_vill = 0
    based_vill = 0
    
    for i in range(num_villagers):
        vill_dict[i + 1] = []
    
    for index, day in enumerate(attendees_lists):
        if 0 in day:
            total_songs += 1
            song_list.append('song' + str(total_songs))
            current_song = 'song' + str(total_songs)
            for person in day:
                if person in vill_dict:
                    vill_dict[person].append(current_song)
        else:
            if index == 0:
                for person in day:
                    if person in vill_dict:
                        vill_dict[person].append('song0')
            else:
                for index, person in enumerate(day):
                    if index <= len(day) - 2:
                        for song in vill_dict[person]:
                            if song not in vill_dict[day[index + 1]]:
                                vill_dict[day[index + 1]].append(song)
    
    for key in vill_dict:
        know_all = 0
        for song in song_list:
            if song in vill_dict[key]:
                know_all += 1
        if know_all == total_songs:
            based_vill +=1
                        
    
    
    return based_vill