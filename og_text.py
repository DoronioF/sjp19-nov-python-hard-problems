"""
Before smart phones, when you wanted to text someone, you had to press the same number over and over again to get a letter from the keypad.

You can see the number 2 has three letters associated with it: A, B, and C. Originally,
you would press the 2 button once to get the letter A, push the 2 button twice to get
the letter B, and push the 2 button three times to get the letter C.

At some point, someone tried to make it easier by including a dictionary in the phone.
You could just type the numbers once, and the phone would try to guess the word for
you.

For example, let's say the phone's dictionary contained the words "ARROW", "BOTOX",
"DOMES", and "FOODS". If you typed the numbers 26869, then the phone would
come up with the word BOTOX for you. If you typed the numbers 27769, then the
phone would come up with the word ARROW for you. If you typed the numbers
36637, it would come up with both the words "DOMES" and "FOODS".

Given that mapping of numbers to letters and a list of valid words, it's up to you to
figure out which of those words could be created from the digits someone types into
the phone.

Given list of valid words and a number digits, complete the function
translate to return the total number of the words in words that can be created
using the digits from digits.
"""
#-------------------------------------Notes-----------------------------------------------
"""
Create a dictionary to contain the values for each number that correlates to each letter

Return the count of words that can be translated to digits
"""
#-------------------------------------Notes-----------------------------------------------

#-----------------------SOLUTION----------------------------------------------------
def translate(words, digits):
    t9 = {
        "2": "ABC",
        "3": "DEF",
        "4": "GHI",
        "5": "JKL",
        "6": "MNO",
        "7": "PQRS",
        "8": "TUV",
        "9": "WXYZ"
    }
    count = 0
    for word in words:
        switch = True
        for i, letter in enumerate([*word]):
            if letter in t9[str(digits)[i]]:
                continue
            else:
                switch = False
        if switch:
            count += 1
    return count