"""
You and your friend decide to meet up in a breakout room, but you don't want other
people to know which breakout room you're going to until after you've chosen it.

To do this, you devise a way to send a secret message. You choose four unique
letters, like "PLOD". Then, you construct a string that has those letters in it, in order,
with other letters possible between the letters that you chose.
You send the four unique letters and the secret message to your friend.

When your friend gets the four letters and the string, they throw out as many letters
as they can to leave only all of the "PLOD" values that they can.
Then, they count how many "PLOD" occurrences there are in the secret message.
That's the number of the breakout room that the two of you should meet in.

---------------------------------------------------PROMPT------------------------------------------------------------------
Given two strings, code and message, complete the function breakout_room
that returns the count of the number of times that code occurs in message.

Constraints:
 * code has four unique uppercase letters
 * 1 <= len(message) <= 100000
 * message has only uppercase letters

Example:
Let's say that you receive these values from your friend.
You scan the string and find all of the ASDF values in it in that order.

code:    ASDF
message: SODIFJOSDIFJASOIDFASLWEARWOERIMSOEIDENRMFASD
                     AS  DFAS               D    F

Your code would return 2.
"""
def breakout_room(code, message):
    code_index = 0
    room_number = 0
    for letter in message:
        if letter == code[code_index]:
            code_index += 1
        if code_index >= len(code):
            room_number += 1
            code_index = 0
    return room_number

